import random
from threading import Lock
from datetime import datetime

import json
from kafka import KafkaConsumer
from flask import Flask, render_template, request
app = Flask(__name__)


consumers = {
    "active_posts": KafkaConsumer('active_posts'),
    "recommendation": KafkaConsumer('recommendation'),
}
posts_lock = Lock()
recmd_lock = Lock()


@app.route('/posts', methods=['GET'])
def posts():
    global posts_lock
    with posts_lock:
        global consumers;
        msg = next(consumers['active_posts'])
        (timestamp, posts) = json.loads(msg.value)
        timestamp = str(datetime.fromtimestamp(timestamp))
        
        # only the top 25 posts are displayed
        #     so as to lighten browser's burden
        if len(posts) > 25: posts = posts[:25]

        return render_template("containers/posts.html",
                               timestamp=timestamp, 
                               posts=posts)

@app.route('/recommendation', methods=['GET'])
def recommendation():
    global recmd_lock
    with recmd_lock:
        global consumers;
        msg = next(consumers['recommendation'])
        _, recommends = json.loads(msg.value)

        # random choice one user to display
        account, users = random.choice(recommends)
        return render_template("containers/recommendation.html",
                               account=account,
                               users=users)
                               

@app.route('/')
def index():
    return render_template("index.html", posts=posts)

if __name__ == '__main__':
    app.run(debug=False, port=5000)
