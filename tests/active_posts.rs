extern crate projects;

use projects::*;
use timely::dataflow::operators::*;

#[test]
fn test_active_posts() {
    timely::execute_from_args(std::env::args(), move |worker| {
        let (mut input, cap) = worker.dataflow::<u64, _, _>(|scope| {
            let (input, stream) = scope.new_unordered_input();
            stream
                .fix()
                .fill()
                .time_window(1800)
                .synthesize(24)
                .inspect(|x| {
                    let (_, posts) = x;
                    assert_eq!(posts.len(), 1);
                    assert_eq!(posts[0].comment_cnt, 2);
                    assert_eq!(posts[0].reply_cnt, 2);
                    if let Some(user_cnt) = posts[0].user_cnt {
                        // [122, 123] 
                        assert_eq!(user_cnt, 2);
                    }
                });
            input 
        });

        let mut records = vec![
            // check: out of order
            RawEvent { 
                event_time: 1328151601, 
                event_type: EventType::Signal, 
                raw: "".to_string() 
            },
            // check: post
            RawEvent { 
                event_time: 1328150816, 
                event_type: EventType::Post, 
                raw: "270250|122||||||||2440|28".to_string() 
            },
            // check comment
            RawEvent { 
                event_time: 1328151016, 
                event_type: EventType::Comment, 
                raw: "1045500|122|||||270250||28".to_string() 
            },
            // check comment
            RawEvent { 
                event_time: 1328151017, 
                event_type: EventType::Comment, 
                raw: "1045501|122|||||270250||28".to_string() 
            },
            // check: replying
            RawEvent { 
                event_time: 1328151017, 
                event_type: EventType::Comment, 
                raw: "1045502|122||||||1045500|28".to_string() 
            },
            // check: replying to reply
            RawEvent { 
                event_time: 1328151018, 
                event_type: EventType::Comment, 
                raw: "1045502|123||||||1045502|28".to_string() 
            },
        ];

        for record in records.drain(..) {
            input.session(cap.delayed(&record.event_time))
                 .give(record);
        }
        drop(input);
        drop(cap);
        while worker.step() {}

    }).unwrap();
}
