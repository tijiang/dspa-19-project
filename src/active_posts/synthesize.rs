/// Input:  bins of events (1 per 30 mins)
/// Output: 
///   1. #comments of each active post, updated per 30 mins
///   2. #replies (comments of comments) of each active posts, updated per 30 mins
///   3. #unique indiviuals involved in each active post

use std::collections::{HashMap, BTreeMap};

use timely::dataflow::operators::generic::operator::Operator;
use timely::dataflow::{Stream, Scope};
use timely::dataflow::channels::pact::Pipeline;

use super::defs::*;

pub trait Synthesize<G: Scope> {
    fn synthesize(&self, size: usize) -> Stream<G, (u64, Vec<SummaryTuple>)>;
}

impl<G: Scope<Timestamp=u64>> Synthesize<G> for Stream<G, (u64, Vec<Event>)> {
    fn synthesize(&self, size: usize) -> Stream<G, (u64, Vec<SummaryTuple>)> {
        let mut vec = Vec::new();
        let mut map = BTreeMap::<TimeStamp, HashMap<PostId, Summary>>::new(); 

        let mut update_engagement = false;

        self.unary_notify(Pipeline, "Synthesize", None, move |input, output, notificator| {
            input.for_each(|time, data| {
                data.swap(&mut vec);

                for datum in vec.drain(..) {
                    let mut stash = HashMap::<PostId, Summary>::new(); 
                    let (t_window, mut events) = datum;
                    for event in events.drain(..) {
                        match event {
                            Event::CommentEvent(event) => {
                                let post_id = event.reply_to_post_id.unwrap();
                                let summary = stash.entry(post_id)
                                                   .or_insert_with(|| { 
                                                       Summary::new(post_id)
                                                   });

                                // if it's a reply
                                if event.reply_to_comment_id.is_some() {
                                    summary.reply_cnt += 1;
                                }
                                else { // otherwise
                                    summary.comment_cnt += 1;
                                }
                                summary.users.insert(event.person_id);
                            },
                            Event::LikeEvent(event) => {
                                let post_id = event.post_id;
                                let summary = stash.entry(post_id)
                                                   .or_insert_with(|| {
                                                       Summary::new(post_id)
                                                   });
                                summary.users.insert(event.person_id);
                                summary.like_cnt += 1;
                            },
                            Event::PostEvent(event) => {
                                let post_id = event.id;
                                let summary = stash.entry(post_id)
                                                   .or_insert_with(|| { 
                                                       Summary::new(post_id)
                                                   });
                                summary.users.insert(event.person_id);
                            }
                            _ => {}
                        }
                    }
                    map.entry(t_window)
                       .or_insert(stash);
                    notificator.notify_at(time.delayed(&t_window));
                }
            });

            notificator.for_each(|cap, _, _| {
                let mut buffer = Vec::new();

                // store the timestamp of the windows
                for k in map.keys() {
                    if k > cap.time() { break; }
                    buffer.push(*k);
                }

                // remove outdated windows
                if buffer.len() > size {
                    for k in &buffer[..(buffer.len() - size)] {
                        map.remove(&k);
                    }
                }

                let mut merged = BTreeMap::new();
                // start from the first unremoved windows
                let l = if buffer.len() > size { buffer.len() - size } else { 0 };
                // merge summaries
                for k in buffer.drain(l..) {
                    if let Some(stash) = map.get(&k) {
                        for (post_id, summary) in stash {
                            merged.entry(post_id)
                                  .or_insert_with(|| Summary::new(*post_id))
                                  .update(summary, update_engagement);
                        }
                    }
                }

                // serialize summaries and send them
                let summaries: Vec<_> = merged.iter()
                                              .map(|(_, v)| v.serialize(update_engagement))
                                              .collect();
                output.session(&cap).give((*cap.time(), summaries));
                update_engagement = !update_engagement;
            });
        })
    }
}
