extern crate projects;

use projects::*;

use timely::dataflow::ProbeHandle;
use timely::dataflow::operators::*;
use rdkafka::config::ClientConfig;
use kafkaesque::EventProducer;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let config = Config::new(&args[1]);

    timely::execute_from_args(std::env::args(), move |worker| {
        // configure kafka
        let brokers = "localhost:9092";
        let mut producer_config = ClientConfig::new();
        producer_config
            .set("bootstrap.servers", brokers);

        let mut probes = vec![ProbeHandle::new(), ProbeHandle::new()];
        let (mut input, mut cap) = worker.dataflow::<u64, _, _>(|scope| {
            let (input, stream) = scope.new_unordered_input();
            let stream = stream
                .fix()
                .inspect(|x: &RawEvent| {
                    if x.event_type != EventType::Signal {
                        println!("\x1b[38;5;196m [Inspect] {:?} \x1b[0m", x);
                    }
                });

            let stream = stream
                .fill();
            stream
                .time_window(1800)
                .synthesize(24)
                .map(|x| {
                    serde_json::to_string(&x).unwrap()
                })
                .probe_with(&mut probes[0])
                .capture_into(EventProducer::new(producer_config.clone(), 
                                                 "active_posts".to_string()));
            stream
                .time_window(3600)
                .sliding_window(4)
                .summarize()
                .recommend(&config)
                .map(|x| {
                    serde_json::to_string(&x).unwrap()
                })
                .probe_with(&mut probes[1])
                .capture_into(EventProducer::new(producer_config.clone(),
                                                 "recommendation".to_string()));
            input
        });

        let iter = RecordSource::new(&config);
        for delayed_event in iter {
            input.session(cap.delayed(&delayed_event.event.event_time))
                 .give(delayed_event.event);

            cap = cap.delayed(&(delayed_event.processing_time - config.max_delay));
            for probe in &probes {
                while probe.less_than(cap.time()) {
                    worker.step();
                }
            }
        }
    }).unwrap();
}
