/// File: lib/sources/record_source.rs
/// 
/// `RecordSource` wraps `RecordIter`, providing features
///   like random delay, speed control and periodic 
///   signals (watermark).
/// 
/// To simulate a realistic stream source, `RecordSource` 
///   serves events proportional to their timestamps. 
///   Additionally, events can be delayed by a bounded 
///   random time which causes the events to be served
///   slightly out-of-order of their timestamps. 

use std::collections::BinaryHeap;
use std::cmp::{min, Reverse};
use std::{thread, time};
use std::iter::Peekable;

use rand;
use rand::distributions::{Distribution, Uniform};

use crate::utils::Config;
use super::defs::*;
use super::record_iterator::RecordIter;

pub struct RecordSource {
    iter: Peekable<RecordIter>,

    // sampling
    dist: Uniform<u64>,
    rng: rand::rngs::ThreadRng,

    // out-of-order stream
    pq: BinaryHeap<Reverse<DelayedEvent>>,

    // speed-up
    speedup: u64,
    cur_time: Option<u64>,

    // periodic signal
    signal: Option<u64>,
}

impl RecordSource {
    pub fn new(config: &Config) -> Self {
        let record_iter = RecordIter::new(&config.path); 

        Self {
            iter: record_iter.peekable(),
            dist: Uniform::new_inclusive(0, config.max_delay),
            rng: rand::thread_rng(),
            pq: BinaryHeap::new(),
            speedup: config.speedup,
            cur_time: None,
            signal: None,
        }
    }

    fn get_bounded_delay(&mut self) -> u64 {
        self.dist.sample(&mut self.rng)
    }
}

impl Iterator for RecordSource {
    type Item = DelayedEvent;

    fn next(&mut self) -> Option<DelayedEvent> {
        let mut cur_delayed_time = self.pq.peek().and_then(|v| Some(v.0.processing_time));
        // we only need to buffer those who could be emitted 
        //   before the first event
        while (cur_delayed_time == None) || 
              (cur_delayed_time > self.iter.peek()
                                           .and_then(|x| Some(x.event_time))) {
            match self.iter.next() {
                Some(event) => {
                    let delayed_time = event.event_time + self.get_bounded_delay();
                    cur_delayed_time = match cur_delayed_time {
                        Some(x) => Some(min(x, delayed_time)),
                        None => Some(delayed_time),
                    };
                    self.pq.push(Reverse(DelayedEvent { 
                        processing_time: delayed_time,
                        event,
                    }));
                }
                None => break,
            }
        }

        // add periodical signal
        while self.pq.peek().is_some() && 
                      self.signal < self.pq.peek().map(|x| x.0.processing_time) {

            let t = self.pq.peek().unwrap().0.processing_time;

            // Signal acts as a watermark
            self.signal = match self.signal {
                None => Some((t + 1799) / 1800 * 1800),
                Some(signal) => Some(signal + 1800),
            };
                
            self.pq.push( Reverse(DelayedEvent{
                processing_time: self.signal.unwrap(),
                event: RawEvent {
                    event_time: self.signal.unwrap(),
                    event_type: EventType::Signal,
                    raw: "".to_string(),
                },
            }));
        };

        self.pq.pop().and_then(|v| {
            // wait until the timestamp for next event
            self.cur_time = match self.cur_time {
                Some(cur_time) => {
                    let duration = (v.0.processing_time - cur_time) / self.speedup;
                    thread::sleep(time::Duration::from_secs(duration));
                    Some(v.0.processing_time)
                },
                None => Some(v.0.processing_time),
            };
            Some(v.0)
        })
    }
}