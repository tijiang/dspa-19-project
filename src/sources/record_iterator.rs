/// File: lib/sources/record_iterators.rs
/// 
/// `RecordIter` wraps files in `streams/` folder into
///   a vector of iterators, making it convenient for
///   other structs to manipulate records with order 
///   of event time (creationDate).

use std::fs::File;
use std::io::{BufReader, BufRead, Lines};
use std::iter::{Peekable, Skip};
use std::convert::TryInto;

use lazy_static::lazy_static;
use regex::Regex;
use chrono::{DateTime, Utc};

use super::defs::*;

type PeekableLines = Peekable<Skip<Lines<BufReader<File>>>>;

/// Wrap datasets
pub struct RecordIter{
    iters : Vec<(usize, EventType, PeekableLines)>,
}

impl RecordIter
{
    pub fn new(dataset_path: &str) -> Self {
        let mut filenames = vec![(EventType::Comment, "comment_event_stream.csv"),
                                 (EventType::Post, "post_event_stream.csv"),
                                 (EventType::Like, "likes_event_stream.csv")];

        let iters : Vec<_> = filenames.drain(..)
                                      .enumerate()
                                      .map(|(idx, (event_type, filename))| {
            let fpath = format!("{}/streams/{}", dataset_path, filename);
            let file = File::open(&fpath).expect("Fail to open file");
            let reader = BufReader::new(file);
            (idx, event_type, reader.lines().skip(1).peekable())
        }).collect();

        Self {
            iters,
        }
    }
}

impl Iterator for RecordIter {
    type Item = RawEvent;

    fn next(&mut self) -> Option<RawEvent> {
        let (idx, event_time) = self.iters
            .iter_mut()
            .map(|(idx, _, iter)| (idx, iter))
            .fold((None, None), |(ia, a), (ib, b)| {
            let val = match b.peek() {
                Some(Ok(line)) => {
                    Some(parse_time(&line))
                }
                _ => None,
            };
            if val.is_some() && (a.is_none() || val < a) {
                (Some(*ib), val)
            }
            else {
                (ia, a)
            }
        });

        match idx{
            Some(idx) => {
                let (_, event_type, iter) = &mut self.iters[idx];
                match iter.next() {
                    Some(Ok(x)) => {
                        Some(RawEvent {
                            event_time: event_time.unwrap().try_into().unwrap(),
                            event_type: *event_type,
                            raw: x,
                        })
                    }
                    _ => None,
                }
            },
            None => None,
        }
    }
}


fn parse_time(s: &str) -> i64 {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(?x)
          \d{4}-\d{2}-\d{2}T
          \d{2}:\d{2}:\d{2}(?:.000)?Z").unwrap();
    }
    let loc = RE.find(s).unwrap();
    let creation_date = &s[loc.start()..loc.end()];
    DateTime::parse_from_rfc3339(creation_date).and_then(|dt| {
        Ok(dt.with_timezone(&Utc))
    }).unwrap().timestamp()
}