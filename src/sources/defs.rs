/// File: lib/sources/defs.rs
/// 
/// Include `EventType`, `RawEvent`, `DelayedEvent` and `Event`
/// 
/// `RawEvent` is a low-level wrapper for events, where events read
///     from files are barely parsed (only type of events and time 
///     of events are extracted). The motivation is to reserve as 
///     much information as possible (so no columns are removed).
/// 
/// `DelayedEvent` is a wrapper for `RawEvent`, where a new field 
///     named `processing_time` is introduced, in order to allow 
///     bounded random delay. This struct has implemented traits
///     like `Ord`, making it possible to be sorted according to
///     `processing_time` (or delayed time).
/// 
/// `Event` is higher-leverl wrapper for events, where useful fields
///     like `person_id` are extracted, while unused fields are 
///     discarded in order to reduce overhead of clone. It's a enum
///     with struct variants, allowing unified api for events with
///     different types.

pub type UserID = u32;
pub type TagID = u32;

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub enum EventType {
    Comment = 0,
    Post = 1,
    Like = 2,
    Signal = 3,
}

#[derive(Debug, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct RawEvent {
    pub event_time: u64,
    pub event_type: EventType,
    pub raw: String,
}

#[derive(Ord, PartialOrd, Eq, PartialEq)]
pub struct DelayedEvent {
    pub processing_time: u64, 
    pub event: RawEvent
}

#[derive(Debug, Clone)]
pub enum Event {
    CommentEvent(CommentEvent),
    LikeEvent(LikeEvent),
    PostEvent(PostEvent),
    SignalEvent(SignalEvent),
}

#[derive(Debug, Clone)]
pub struct CommentEvent {
    pub creation_date: u64,
    pub id: u64,
    pub person_id: UserID,
    pub reply_to_post_id: Option<u64>,
    pub reply_to_comment_id: Option<u64>,
    pub place_id: u32,
    pub tags: Vec<TagID>,
}

#[derive(Debug, Clone)]
pub struct LikeEvent {
    pub creation_date: u64,
    pub person_id: UserID,
    pub post_id: u64,
}

#[derive(Debug, Clone)]
pub struct PostEvent {
    pub creation_date: u64,
    pub id: u64,
    pub person_id: UserID,
    pub tags: Vec<TagID>,
    pub forum_id: u32,
    pub place_id: u32,
}

#[derive(Debug, Clone)]
pub struct SignalEvent {
    creation_date: u64,
}

impl Event {
    pub fn parse(raw_event: &RawEvent) -> Self {
        let vec: Vec<&str> = raw_event.raw.split('|').collect();
        match raw_event.event_type {
            EventType::Comment => {
                Event::CommentEvent(CommentEvent {
                    id: vec[0].parse().unwrap(),
                    person_id: vec[1].parse().unwrap(),
                    creation_date: raw_event.event_time,
                    reply_to_post_id: vec[6].parse().ok(),
                    reply_to_comment_id: vec[7].parse().ok(),
                    place_id: vec[8].parse().unwrap(),
                    tags: Vec::new(),
                })
            },

            EventType::Like => {
                Event::LikeEvent(LikeEvent {
                    person_id: vec[0].parse().unwrap(),
                    post_id: vec[1].parse().unwrap(),
                    creation_date: raw_event.event_time,
                })
            },

            EventType::Post => {
                Event::PostEvent(PostEvent {
                    id: vec[0].parse().unwrap(),
                    person_id: vec[1].parse().unwrap(),
                    creation_date: raw_event.event_time,
                    tags: if vec[8].is_empty() {
                        Vec::new()
                    }
                    else {
                        let pat: &[_] = &['[', ']'];
                        vec[8].trim_matches(pat)
                              .split(", ")
                              .map(|x| x.parse().unwrap())
                              .collect()
                    },
                    forum_id: vec[9].parse().unwrap(),
                    place_id: vec[10].parse().unwrap(),
                })
            },

            EventType::Signal => {
                Event::SignalEvent(SignalEvent {
                    creation_date: raw_event.event_time,
                })
            },
        }
    }

    pub fn event_time(&self) -> u64 {
        match self {
            Event::CommentEvent(event) => event.creation_date,
            Event::PostEvent(event) => event.creation_date,
            Event::LikeEvent(event) => event.creation_date,
            Event::SignalEvent(event) => event.creation_date,
        }
    }
}