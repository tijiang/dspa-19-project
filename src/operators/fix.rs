/// Fix the stream by remove counterfactual records
/// 
/// Namely:
///   + all the posts are legal
///   + a comment is legal only when it replies to an existed post
///   + a reply is legal only when it reples to a legal comment
///   + a like is legal only when it "likes" an existed post
/// 
/// Input:  Stream of `RawEvent`
/// Output: Stream of `RawEvent`
///           with counterfactual records removed
/// 

use std::collections::HashMap;

use timely::dataflow::operators::generic::operator::Operator;
use timely::dataflow::{Stream, Scope};
use timely::dataflow::channels::pact::Pipeline;

use super::defs::*;

pub trait Fix<G: Scope> {
    fn fix(&self) -> Stream<G, RawEvent>;
}

impl<G: Scope<Timestamp=u64>> Fix<G> for Stream<G, RawEvent> {
    fn fix(&self) -> Self {
        let mut vec = Vec::new();
        let mut stash = HashMap::new();

        let mut posts = HashMap::new();
        let mut comment_id_to_post_id = HashMap::new();

        self.unary_notify(Pipeline, "Fix", None, move |input, output, notificator| {
            input.for_each(|time, data| {
                data.swap(&mut vec);
                for datum in vec.drain(..) {
                    stash.entry(time.time().clone())
                         .or_insert_with(Vec::new)
                         .push(datum);
                }
                notificator.notify_at(time.retain());
            });

            notificator.for_each(|cap, _, _| {
                if let Some(events) = stash.remove(cap.time()) {
                    for raw_event in events {
                        let event = Event::parse(&raw_event);
                        match event {
                            Event::CommentEvent(event) => {
                                if event.reply_to_comment_id.is_some() {
                                    // case: reply
                                    let comment_id = event.reply_to_comment_id.unwrap();
                                    if comment_id_to_post_id.contains_key(&comment_id) {
                                        let post_id = comment_id_to_post_id[&comment_id];
                                        comment_id_to_post_id.entry(event.id)
                                                             .or_insert(post_id);
                                    }
                                    else { // drop it otherwise
                                        continue;
                                    }
                                }
                                else {
                                    // case: comment
                                    let post_id = event.reply_to_post_id.unwrap();
                                    if posts.contains_key(&post_id) {
                                        comment_id_to_post_id.entry(event.id)
                                                             .or_insert(post_id);
                                    } 
                                    else { // drop it otherwise
                                        continue;
                                    }
                                }
                            },
                            Event::LikeEvent(event) => {
                                if !posts.contains_key(&event.post_id) {
                                    continue;
                                }
                            },
                            Event::PostEvent(event) => {
                                posts.entry(event.id)
                                     .or_insert(true);
                            },
                            _ => {},
                        }
                        output.session(&cap).give(raw_event);
                   }
                }
            });
        })
    }
}