/// Includes implementation of `sliding_window` and 
///     `timedWindow` for `Event`

use std::collections::{HashMap, BTreeMap};

use timely::dataflow::operators::generic::operator::Operator;
use timely::dataflow::{Stream, Scope};
use timely::dataflow::channels::pact::Pipeline;

use crate::defs::Event;

pub trait TimeWindow<G: Scope> {
    fn time_window(&self, timeout: u64) -> Stream<G, (u64, Vec<Event>)>;
}

impl<G: Scope<Timestamp=u64>> TimeWindow<G> for Stream<G, Event> {
    fn time_window(&self, timeout: u64) -> Stream<G, (u64, Vec<Event>)> {
        let mut vec = Vec::new();
        let mut stash = HashMap::new();

        self.unary_notify(Pipeline, "TimeWindow", None, move |input, output, notificator| {
            input.for_each(|time, data| {
                data.swap(&mut vec);

                for datum in vec.drain(..) {
                    // assign a bin every 30 mins
                    let bin_id = datum.event_time() / timeout * timeout; 

                    if !stash.contains_key(&bin_id) {
                        // remove window later
                        let terminate = bin_id + timeout;
                        notificator.notify_at(time.delayed(&terminate));
                    }

                    stash.entry(bin_id)
                         .or_insert_with(Vec::new)
                         .push(datum);
                }
            });

            notificator.for_each(|cap, _, _| {
                let bin_id = *cap.time() - timeout;
                if let Some(list) = stash.remove(&bin_id) {
                    output.session(&cap).give((bin_id + timeout, list));
                }
            });
        })
    }
}


pub trait SlidingWindow<G: Scope, T> {
    fn sliding_window(&self, size: usize) -> Stream<G, (u64, Vec<T>)>;
}

impl<G: Scope<Timestamp=u64>, T: 'static + Clone> SlidingWindow<G, T> for Stream<G, (u64, Vec<T>)> {
    fn sliding_window(&self, size: usize) -> Self {
        let mut vec = Vec::new();
        let mut stash = BTreeMap::<u64, Vec<T>>::new(); 

         self.unary_notify(Pipeline, "SlidingWindow", None, move |input, output, notificator| {
            input.for_each(|time, data| {
                data.swap(&mut vec);

                for datum in vec.drain(..) {
                    let (t_window, mut events) = datum;
                    stash.entry(*time.time())
                         .or_insert_with(Vec::new)
                         .append(&mut events);
                    notificator.notify_at(time.delayed(&t_window));
                }
            });

            notificator.for_each(|cap, _, _| {
                let mut buffer = Vec::new();
                for k in stash.keys() {
                    if k > cap.time() { break; }
                    buffer.push(*k);
                }

                let mut merge = Vec::<T>::new();
                let start_idx = if buffer.len() > size { buffer.len() - size } else { 0 };
                for k in buffer.drain(start_idx..) {
                    if let Some(v) = stash.get(&k) {
                        merge.append(&mut v.clone());
                    }
                }

                for k in buffer.drain(..) {
                    stash.remove(&k);
                }

                output.session(&cap).give((*cap.time(), merge))
            });
         })
    }
}