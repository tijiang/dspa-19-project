/// Preprocess `RawEvent` into an analysis-friendly format
///     Namely, 
///       1) add `reply_to_post_id` field for replies
///       2) add `tags` field for comments & replies, which 
///           is the topics of the post it replies to
/// 
/// Input:  Stream of `RawEvent`
/// Output: Stream of `Event` 

use std::collections::HashMap;

use timely::dataflow::operators::generic::operator::Operator;
use timely::dataflow::{Stream, Scope};
use timely::dataflow::channels::pact::Pipeline;

use super::defs::*;

pub trait Fill<G: Scope> {
    fn fill(&self) -> Stream<G, Event>;
}

impl<G: Scope<Timestamp=u64>> Fill<G> for Stream<G, RawEvent> {
    fn fill(&self) -> Stream<G, Event> {
        let mut vec = Vec::new();
        let mut stash = HashMap::new();

        let mut post_to_tags = HashMap::new();
        let mut comment_id_to_post_id = HashMap::new();

        self.unary_notify(Pipeline, "Fill", None, move |input, output, notificator| {
            input.for_each(|time, data| {
                data.swap(&mut vec);
                for raw_event in vec.drain(..) {
                    if raw_event.event_type != EventType::Comment {
                        let event = Event::parse(&raw_event);
                        match &event {
                            Event::PostEvent(event) => {
                                post_to_tags.entry(event.id.clone())
                                            .or_insert_with(|| event.tags.clone());
                            },
                            _ => {}
                        };
                        output.session(&time).give(event);
                    }
                    else {
                        stash.entry(time.time().clone())
                             .or_insert_with(Vec::new)
                             .push(Event::parse(&raw_event));
                    }
                }
                notificator.notify_at(time.retain());
            });

            notificator.for_each(|cap, _, _| {
                if let Some(events) = stash.remove(cap.time()) {
                    for event in events {
                        let event = match event {
                            Event::CommentEvent(mut comment) => {
                                if comment.reply_to_comment_id.is_some() {
                                    // case: reply
                                    let comment_id = comment.reply_to_comment_id
                                                            .unwrap();
                                    if comment_id_to_post_id.contains_key(&comment_id) {
                                        let post_id = comment_id_to_post_id[&comment_id];
                                        comment.reply_to_post_id = Some(post_id);
                                        comment_id_to_post_id.entry(comment.id)
                                                             .or_insert(post_id);
                                    }
                                    else { // should never be here
                                        panic!(); 
                                    }
                                }
                                else {
                                    // case: comment
                                    let post_id = comment.reply_to_post_id.unwrap();
                                    comment_id_to_post_id.entry(comment.id)
                                                         .or_insert(post_id);
                                }
                                assert_ne!(comment.reply_to_post_id, None);
                                comment.tags = post_to_tags[&comment.reply_to_post_id.unwrap()]
                                                           .clone();
                                Event::CommentEvent(comment)
                            },
                            _ => event,
                        };
                        output.session(&cap).give(event);
                   }
                }
            });
        })
    }
}