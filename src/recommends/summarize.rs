/// GROUP events BY users. 
/// The output contains all the topics a user has discussed.

use std::collections::HashMap;

use timely::dataflow::operators::*;
use timely::dataflow::{Stream, Scope};

use super::defs::*;

pub trait Summarize<G: Scope> {
    fn summarize(&self) -> Stream<G, (u64, Vec<UserSummary>)>;
}

impl<G: Scope<Timestamp=u64>> Summarize<G> for Stream<G, (u64, Vec<Event>)> {
    fn summarize(&self) -> Stream<G, (u64, Vec<UserSummary>)> {
        self.map(|(t, mut events)| {
            let mut stash = HashMap::new();
            for event in events.drain(..) {
                match event {
                    Event::CommentEvent(event) => {
                        stash.entry(event.person_id)
                             .or_insert_with(|| UserSummary::new(event.person_id))
                             .discussed_tags
                             .extend(event.tags);
                    },
                    Event::LikeEvent(event) => {
                        stash.entry(event.person_id)
                             .or_insert_with(|| UserSummary::new(event.person_id));
                    },
                    Event::PostEvent(event) => {
                        stash.entry(event.person_id)
                             .or_insert_with(|| UserSummary::new(event.person_id))
                             .discussed_tags
                             .extend(event.tags);
                    },
                    _ => {}

                }
            }

            (t,  stash.drain().map(|(_, v)| v).collect())
        })
    }
}

#[derive(Clone)]
pub struct UserSummary {
    pub person_id: u32,
    pub discussed_tags: Vec<TagID>,
}

impl UserSummary {
    pub fn new(person_id: u32) -> Self {
        Self {
            person_id,
            discussed_tags: Vec::new(),
        }
    } 
}