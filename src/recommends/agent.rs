// All the functions that related to the computation of the similarity
//   are moved here, in order to make the logic of `Recommend` easier to read.
// See `recommend.rs` for detailed documentation.

use super::defs::*;
use super::utils::*;

use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;


pub struct SimilarityAgent {
    interested_tagclasses: HashMap<UserID, HashSet<TagID>>,
    known: HashMap<UserID, HashSet<UserID>>,
    tag_has_tagclass: HashMap<TagID, TagID>,
    tagclass_is_subclass_of_tagclass: HashMap<TagID, TagID>,
    has_closure_computed: HashSet<UserID>,
}

impl SimilarityAgent {
    pub fn new(path: &str) -> Self {
        let p_table = format!("{}/tables", path);

        // Load mapping: tag -> tagclass
        let p_tagclass = format!("{}/tag_hasType_tagclass.csv", p_table);
        let tag_has_tagclass: HashMap<TagID, TagID> = parse_injection(&p_tagclass);

        // Load tags and map tags to tagclasses
        let p_tags = format!("{}/person_hasInterest_tag.csv", p_table);
        let interested_tagclasses = parse_surjection_with_f(&p_tags, 
                                                            &tag_has_tagclass);

        // Compute closure under inclusion
        let p_subclass = format!("{}/tagclass_isSubclassOf_tagclass.csv", p_table);
        let tagclass_is_subclass_of_tagclass = parse_injection(&p_subclass);

        // Load known person
        let p_known = format!("{}/person_knows_person.csv", p_table);
        let known: HashMap<UserID, HashSet<UserID>> = parse_surjection(&p_known);

        Self {
            interested_tagclasses,
            known,
            tag_has_tagclass,
            has_closure_computed: HashSet::new(),
            tagclass_is_subclass_of_tagclass,
        }
    }

    pub fn is_known(&self, a: &UserID, b: &UserID) -> bool {
        self.known[a].contains(b)
    }

    // first compute the closure, and then jaccard index of these two closures
    pub fn tag_similarity(&mut self, a: UserID, b: UserID) -> f64 {
        if !self.has_closure_computed.contains(&a) {
            self.compute_closure(a);
        }

        if !self.has_closure_computed.contains(&b) {
            self.compute_closure(b);
        }

        jaccard_index(&self.interested_tagclasses.get(&a).unwrap_or(&HashSet::new()), 
                      &self.interested_tagclasses.get(&b).unwrap_or(&HashSet::new()))
    }

    // jaccard index of common friends
    pub fn friend_similarity(&self, a: UserID, b: UserID) -> f64 {
        jaccard_index(&self.known.get(&a).unwrap_or(&HashSet::new()),
                      &self.known.get(&b).unwrap_or(&HashSet::new()))
    }

    // It's called twice during computation: 
    //   for the first time: (user_a, user_b)
    //   for the second time: (user_b, user_a) 
    // The final result is the sum, so that this criterion would be symmetric
    pub fn discussed_tag_similarity(&mut self, a: UserID, b: &Vec<TagID>) -> f64 {
        let mut set = HashSet::from_iter(b.iter().map(|v| self.tag_has_tagclass[v]));
        compute_closure(&self.tagclass_is_subclass_of_tagclass, &mut set);

        if !self.has_closure_computed.contains(&a) {
            self.compute_closure(a);
        }
        jaccard_index(&self.interested_tagclasses.get(&a).unwrap_or(&HashSet::new()), 
                      &set)
    }

    /// Compute the closure under inclusion
    fn compute_closure(&mut self, user: UserID) {
        // to avoid borrowing `self` as mutable reference, remove the set and then 
        //   insert it back
        if let Some(mut e) = self.interested_tagclasses.remove(&user) {
            compute_closure(&self.tagclass_is_subclass_of_tagclass, &mut e);
            self.interested_tagclasses.entry(user)
                                      .or_insert(e);
        }
        self.has_closure_computed.insert(user);
    }
}


pub fn compute_closure<T>(map: &HashMap<T, T>, set: &mut HashSet<T>)
where T: Eq + Ord + PartialEq + PartialOrd + std::hash::Hash + Clone
{
    let mut s = HashSet::new();
    for element in set.iter() {
        let mut t = element;
        s.insert(t.clone());
        while map.contains_key(t) {
            t = &map[t];
            s.insert(t.clone());
        }
    }

    for element in s.drain() {
        set.insert(element);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_closure() {
        let path = "./datasets/1k-users-sorted";
        let agent = SimilarityAgent::new(path);

        let mut a = HashSet::new();
        a.insert(74); // 74: SoccerPlayer
        compute_closure(&agent.tagclass_is_subclass_of_tagclass, 
                        &mut a);

      
        // 149: Athelete
        // 211: Person
        // 239: Agent
        // 0: Thing
        let b: HashSet<_> = [74, 149, 211, 239, 0].iter().cloned().collect();
        assert_eq!(a, b);
    }

    #[test]
    fn test_similarity() {
        let path = "./datasets/1k-users-sorted";
        let mut agent = SimilarityAgent::new(path);
        let sim = agent.tag_similarity(757, 775);

        // tag: 757 => [1419] (Guy_Sebastian  => MusicalArtist)
        //   => (115, 250, 211, 239, 0)
        // tag: 775 => [1536] (Fidel_V._Ramos => OfficeHolder)
        //   => (349, 211, 239, 0)
        assert_eq!(sim, 3.0 / 6.0);
    }

    #[test]
    fn test_discussed_tag_similarity() {
        let path = "./datasets/1k-users-sorted";
        let mut agent = SimilarityAgent::new(path);
        let sim = agent.discussed_tag_similarity(757, &vec![1536]);

        // tag: 757 => [1419] (Guy_Sebastian  => MusicalArtist)
        //   => (115, 250, 211, 239, 0)
        // tag: 775 => [1536] (Fidel_V._Ramos => OfficeHolder)
        //   => (349, 211, 239, 0)
        assert_eq!(sim, 3.0 / 6.0);
    }
}