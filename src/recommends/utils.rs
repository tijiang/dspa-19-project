use std::fs::File;
use std::io::{BufReader, BufRead};
use std::collections::{HashMap, HashSet};


pub fn jaccard_index<T>(a: &HashSet<T>, b: &HashSet<T>) -> f64 
where T: Eq + std::hash::Hash + std::fmt::Debug
{
    let union: HashSet<_> = a.union(b).collect();
    let inter: HashSet<_> = a.intersection(b).collect();
    let index: f64 = inter.len() as f64 / union.len() as f64;
    index
}

pub fn parse_surjection<T>(fpath: &str) -> HashMap<T, HashSet<T>>
where
    T: std::hash::Hash + Eq + std::str::FromStr
{
    let mut map = HashMap::new();

    for line in get_file_iter(fpath) {
        if let Ok(line) = line {
            let v: Vec<&str> = line.split('|').collect();
            let a: T = v[0].parse().ok().expect("a should be parsable");
            let b: T = v[1].parse().ok().expect("b should be parsable");
            map.entry(a)
               .or_insert_with(HashSet::new)
               .insert(b);
        }
    }
    map
}

pub fn parse_surjection_with_f<T>(fpath: &str, f: &HashMap<T, T>) -> HashMap<T, HashSet<T>> 
where
    T: std::hash::Hash + Eq + std::str::FromStr + Clone
{
    let mut map = HashMap::new();

    for line in get_file_iter(fpath) {
        if let Ok(line) = line {
            let v: Vec<&str> = line.split('|').collect();
            let a: T = v[0].parse().ok().expect("a should be parsable");
            let b: T = v[1].parse().ok().expect("b should be parsable");
            map.entry(a)
               .or_insert_with(HashSet::new)
               .insert(f[&b].clone());
        }
    }
    map
}

pub fn parse_injection<T>(fpath: &str) -> HashMap<T, T> 
where
    T: std::hash::Hash + Eq + std::str::FromStr
{
    let mut map = HashMap::new();

    for line in get_file_iter(fpath) {
        if let Ok(line) = line {
            let v: Vec<&str> = line.split('|').collect();
            let a: T = v[0].parse().ok().expect("a should be parsable");
            let b: T = v[1].parse().ok().expect("b should be parsable");
            map.entry(a)
               .or_insert(b);
        }
    }
    map
}


fn get_file_iter(fpath: &str) -> impl Iterator<Item = Result<String, std::io::Error>> {
    let file = File::open(fpath).expect("Fail to open file");
    let reader = BufReader::new(file);
    reader.lines().skip(1)
}