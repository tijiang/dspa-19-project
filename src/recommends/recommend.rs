/// Compute the similarity based on Jaccard Index
/// 
/// The similarity consists of 3 parts:
///     1. jaccard index of interested tags (common interested tags)
///     2. jaccard index of friends (common friends)
///     3. jaccard index of recently discussed topics 
///         - jaccard_index(a.interested_tags, b.discussed_tags) + 
///           jaccard_index(b.interested_tags, a.discussed_tags)
///         - the motivation is that, a vip (users we need to generate list for) 
///             is often not active during the past 4 hours, so naively compute
///             the similarity of "discussed tags" won't get any meaningful result
/// 
/// To compute the jaccard index of two sets of tags, 
///     1. lookup the tagclass a tag belongs to,
///     2. collect all the ancesters of the tagclass (e.g. Politicians -> Person -> Thing)
///     3. add these tagclassed to a set
/// In this way, we are able to take semantics relation into consideration
///     


use std::collections::{BinaryHeap, HashMap};
use std::cmp::Ordering;

use timely::dataflow::operators::generic::operator::Operator;
use timely::dataflow::{Stream, Scope};
use timely::dataflow::channels::pact::Pipeline;
use serde::{Serialize, Deserialize};

use super::defs::*;
use super::agent::*;
use crate::utils::Config;


pub trait Recommend<G: Scope> {
    fn recommend(&self, config: &Config) -> Stream<G, (u64, Vec<(UserID, Vec<Score>)>)>;
}

impl<G: Scope<Timestamp=u64>> Recommend<G> for Stream<G, (u64, Vec<UserSummary>)> {
    fn recommend(&self, config: &Config) -> Stream<G, (u64, Vec<(UserID, Vec<Score>)>)> {
        let mut vec = Vec::new();

        // Initialize an agent for computing similarity
        let mut agent = SimilarityAgent::new(&config.path);

        // Load VIPs
        let mut to_follow: Vec<_> = config.users.iter().map(|x| {
            (*x, BinaryHeap::<Score>::new())
        }).collect();

        let mut stash = HashMap::new();

        self.unary_notify(Pipeline, "Recommmend", None, move |input, output, notificator| {
            input.for_each(|time, data| {
                data.swap(&mut vec);

                for datum in vec.drain(..) {
                    let (t_window, summaries) = datum;

                    for (vip, heap) in to_follow.iter_mut() {
                        for summary in &summaries {
                            let user = summary.person_id;

                            // filter known person
                            if agent.is_known(vip, &user) { continue; }
                            // update vip's information if vip == user
                            if *vip == user {
                                let map = stash.entry(t_window)
                                               .or_insert_with(HashMap::new);
                                map.entry(*vip)
                                   .or_insert_with(|| summary.discussed_tags.clone());
                                continue;
                            }

                            let common_tag_index = agent.tag_similarity(*vip, 
                                                                        user);
                            let common_friend_index = agent.friend_similarity(*vip, 
                                                                               user);
                            // discussed_tag_index includes two parts:
                            //   similarity(*vip, &summary.discussed_tags) +
                            //   similarity(summary.person_id, &vip.discussed_tags)
                            // for now, only part of the score is computed, and we will
                            //   need to compute the remained half in the notificator
                            //   as events could arrive out of order
                            let discussed_tag_index = agent.
                                discussed_tag_similarity(*vip, &summary.discussed_tags);

                            heap.push(Score::new(
                                t_window,
                                summary.person_id,
                                common_tag_index,
                                common_friend_index,
                                discussed_tag_index + 1.01,
                            ));
                        }
                    }

                    notificator.notify_at(time.delayed(&t_window));
                }
            });

            notificator.for_each(|cap, _, _| {
                let mut list = Vec::new();
                for (vip, heap) in to_follow.iter_mut() {
                    let mut v = HashMap::new();
                    let mut buffer = Vec::new();
                    while let Some(mut pivot) = heap.pop() {
                        // case #0: the pivot comes from future
                        if pivot.timestamp > *cap.time() {
                            // ignore it for now, but don't drop it
                            buffer.push(pivot);
                        }
                        // case #1: the pivot is up to date
                        else if pivot.timestamp == *cap.time() {
                            if pivot.discussed_tag_index > 1.0 {
                                // the discussed tag index is only partly computed
                                pivot.discussed_tag_index -= 1.01;

                                let tags = stash.get(cap.time())
                                                .and_then(|x| x.get(vip));
                                if let Some(tags) = tags {
                                    pivot.discussed_tag_index += 
                                        agent.discussed_tag_similarity(pivot.person_id, 
                                                                       &tags);
                                }
                                pivot.discussed_tag_index /= 2.0;
                                pivot.compute_score();
                                heap.push(pivot);
                            }
                            else {
                                v.entry(pivot.person_id)
                                 .or_insert_with(|| pivot.clone());
                                buffer.push(pivot);
                            }
                        }
                        // case #2: the user is still active, but is not up to date
                        else if pivot.timestamp >= cap.time() - 8 * 3600 {
                            // recompute the score, as we are only allowed to use
                            //   activity during the past 4 hours
                            if pivot.discussed_tag_index != 0.0 {
                                pivot.discussed_tag_index = 0.0;
                                pivot.compute_score();
                                heap.push(pivot);
                            }
                            else {
                                v.entry(pivot.person_id)
                                 .or_insert_with(|| pivot.clone());
                                buffer.push(pivot);
                            }
                        }
                        else { // case #3: the user is no longer active, discard it
                            continue;
                        }

                        if v.len() >= 5 { break; }
                    }
                    let mut v: Vec<_> = v.drain().map(|(_, v)| v).collect();
                    v.sort_by(|a, b| b.cmp(a));
                    list.push((*vip, v));

                    // restore temporarily removed elements
                    for pivot in buffer.drain(..) {
                        heap.push(pivot);
                    }
                }
                stash.remove(cap.time());
                output.session(&cap).give((*cap.time(), list));
            });
        })
    }
}

#[derive(PartialOrd, PartialEq, Clone, Serialize, Deserialize)]
pub struct Score {
    pub value: f64,
    pub timestamp: u64,
    pub person_id: u32,
    pub common_tag_index: f64,
    pub common_friend_index: f64,
    pub discussed_tag_index: f64,
}

impl Score {
    pub fn new(timestamp: u64, 
               person_id: u32, 
               common_tag_index: f64, 
               common_friend_index: f64,
               discussed_tag_index: f64) -> Self {
        
        let mut score = Self {
            value: 0.0,
            timestamp,
            person_id,
            common_tag_index,
            common_friend_index,
            discussed_tag_index,
        };
        score.compute_score();
        score
    }

    pub fn compute_score(&mut self) {
        self.value = 0.3 * self.common_tag_index + 
                     0.4 * self.common_friend_index + 
                     0.3 * self.discussed_tag_index;
    }
}

impl Eq for Score {}
impl Ord for Score {
    fn cmp(&self, other: &Score) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}