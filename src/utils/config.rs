// Parse .toml

use std::fs;

use serde::{Serialize, Deserialize};


#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub path : String,
    pub speedup : u64,
    pub max_delay: u64,
    pub users: Vec<u32>,
}

impl Config {
    pub fn new(path: &str) -> Self {
        let toml_str = fs::read_to_string(path).unwrap();
        toml::from_str(&toml_str).unwrap()
    }
}