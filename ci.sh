# Uncomment to treat warnings as errors
# cargo clippy -- -D warnings -W clippy::pedantic
cargo clippy -- -W clippy::pedantic
