import sys
import  os

import numpy as np
from IPython import embed


path = sys.argv[1]
def read(fn, encoding="latin9"):
    fn = os.path.join(path, fn)
    with open(fn, encoding=encoding) as f:
        return f.read().splitlines()[1:]

def parse_tag(tags):
    return dict(map(lambda x: (int(x.split('|')[0]), x.split('|')[1]),
                    tags))

def parse_tagclass_subset(tagclass_subset):
    children = {}
    for line in tagclass_subset:
        child, parent = tuple(map(int, line.split('|')))
        children.setdefault(parent, [])\
                .append(child)
    return children

def display_tag_treeview(tagclass, tagclass_subset):
    def display(node, level):
        print("\t"*level + tagclass[node])
        for c in tagclass_subset.get(node, []):
            display(c, level+1)
    display(0, 0)

def parse_person(person):
    d = {}
    for line in person:
        idx, vorname, nachname = line.split("|")[:3]
        idx, name = int(idx), " ".join([vorname, nachname])
        d[idx] = name
    return d

def parse_person_tag(person_tag):
    person_to_tags = {}
    for line in person_tag:
        person, tag = map(int, line.split('|'))
        person_to_tags.setdefault(person, [])\
                      .append(tag)
    return person_to_tags

def display_person_tag(person_2_tag, tags, person):
    assert(len(person) == len(person_2_tag))
    arr = np.asarray([len(v) for (_, v) in person_2_tag.items()])
    print("-- avg:\t", arr.mean())
    print("-- max:\t", arr.max(), "/", len(tags))
    print("-- q1:\t", np.percentile(arr, 25), "/", len(tags))
    print("-- q2:\t", np.percentile(arr, 50), "/", len(tags))
    print("-- q3:\t", np.percentile(arr, 75), "/", len(tags))
    print("-- min:\t", arr.min(), "/", len(tags))

if __name__ == "__main__":
    print("Visualize tagclass")
    tagclass = parse_tag(read("tagclass.csv"))
    tagclass_subset = parse_tagclass_subset(read("tagclass_isSubclassOf_tagclass.csv"))
    tagclass_parent = parse_tag(read("tagclass_isSubclassOf_tagclass.csv"))
    display_tag_treeview(tagclass, tagclass_subset)

    print("Avg #tags per Person")
    people = parse_person(read("person.csv"))
    tags = parse_tag(read("tag.csv"))
    tag_2_tagclass = parse_tag(read("tag_hasType_tagclass.csv"))
    person_2_tag = parse_person_tag(read("person_hasInterest_tag.csv"))
    display_person_tag(person_2_tag, tags, people)

    embed()

