./bin/zookeeper-server-start.sh config/zookeeper.properties &
sleep 3s
./bin/kafka-server-start.sh config/server.properties &
sleep 3s
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic active_posts
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic recommendation

